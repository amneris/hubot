# Description:
#   Allows hubot to answer almost any question by asking Wolfram Alpha
#
# Dependencies:
#   "wolfram-alpha": "0.3.0"
#
# Configuration:
#   HUBOT_WOLFRAM_APPID - your AppID
#
# Commands:
#   hubot question <question> - What you want to calculate or know about?
#
# Author:
#   dhorrigan

Wolfram = require('wolfram-alpha').createClient(process.env.HUBOT_WOLFRAM_APPID)

module.exports = (robot) ->
  robot.respond /(question|wfa|wolfram) (.*)$/i, (msg) ->
#console.log msg.match
    Wolfram.query msg.match[2], (e, result) ->
# console.log result
      if result and result.length > 0
        msg.send result[1]['subpods'][0]['text']
      else
        msg.send 'Hmm...not sure'
