# Description:
#   Send message to chatroom using HTTP POST
#
# Borrowed from:
# https://leanpub.com/automation-and-monitoring-with-hubot/read#leanpub-auto-serving-http-requests
#
# URLS:
#   POST /hubot/notify/<room>

module.exports = (robot) ->
  robot.router.get '/hubot/notify/:room', (req, res) ->
    room = req.params.room
    robot.messageRoom room, "```\n#{JSON.stringify(req.query, null, 4)}\n```";
    res.end()

  robot.router.post '/hubot/notify/:room', (req, res) ->
    room = req.params.room
    robot.messageRoom room, "```\n#{JSON.stringify(req.body, null, 4)}\n```";
    res.end()
