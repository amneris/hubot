
module.exports = (robot) ->
  robot.catchAll (msg) ->
    r = new RegExp "^@?(?:#{robot.alias}|#{robot.name}):? (.*)", "i"
    matches = msg.message.text.match(r)
    if matches != null && matches.length > 1
      msg.send "I don't know how to react to: *#{matches[1]}*"

    msg.finish()
