# Description:
#   Send message to Dashboard
#
# Configure:
#   DASHING_HOST - Path to the dashboard
#   DASHING_AUTH_KEY - Auth key for the dashboard
# Commands:
#   hubot dashboard <widget> <dirty-json> - Sends json to widget in ABA's dashboard

dJSON = require('dirty-json');

module.exports = (robot) ->

  robot.respond /dashboard ([\w]+) (.*)/i, (msg) ->
    sendEventToDashboard msg, msg.match[1].trim(), msg.match[2].trim()

  # Send message to the "welcome" widget
  sendEventToDashboard = (msg, widget, body) ->
      dJSON.parse("{#{body}}")
      .then (json) ->
        json['auth_token'] = process.env.DASHING_AUTH_KEY
        data = JSON.stringify(json)
        try
          msg.http("#{process.env.DASHING_HOST}/widgets/#{widget}")
          .post(data) (err, res, body) ->
            if !err
              msg.send body
            else
              msg.send err
        catch error
          console.log error
          msg.send "unable to connect :("
      .then null, (err) ->
        msg.send err
